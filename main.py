#!/usr/bin/python3

import os

import numpy as np
from automaton.ECA import elementary_cellular_automaton as eca


def save(path, dataset):

    if os.path.isfile(path):
        dataset_load = list(np.load(path))
        new_dataset = dataset + dataset_load
        np.save(path, np.array(new_dataset))

    else:
        np.save(path, np.array(dataset))


def main():
    path = "/home/qwerty/Documentos/eca/"
    for rule in range(0, 256):
        dataset = []
        for i in range(10):
            dataset.append(eca(rule, 100, 100))

        save(path + f"{rule}.npy", dataset)


if __name__ == "__main__":
    main()
