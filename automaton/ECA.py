#!/usr/bin/python3

import numpy as np
from scipy.signal import correlate2d
from matplotlib import pyplot as plt

STATE = []


def _get_list_state(rule):
    global STATE
    STATE = [int(x) for x in format(rule, "08b")]
    STATE.reverse()
    STATE = set([i * j for i, j in zip(range(0, len(STATE)), STATE)])
    STATE.remove(0)
    STATE = list(STATE)


def _next_generetion(generation):
    next_generation = correlate2d(
        np.array([generation]), np.array([[4, 2, 1]]), "same", "wrap"
    )[0]
    next_generation[~np.isin(next_generation, STATE)] = 0
    next_generation[np.isin(next_generation, STATE)] = 1

    return next_generation


def _plot_matrix(matrix, title):
    plt.figure()
    plt.imshow(matrix, cmap="Greys", interpolation="nearest")
    plt.title(title)
    plt.show()


def elementary_cellular_automaton(rule=30, steps=100, width=100):

    _get_list_state(rule)

    matrix = np.zeros((steps, width), dtype=np.uint8)
    matrix[0] = np.random.randint(2, size=(1, width))[0]

    for step in range(steps - 1):
        matrix[step + 1] = _next_generetion(matrix[step])

    return matrix


if __name__ == "__main__":
    matrix = elementary_cellular_automaton(rule=182, steps=100, width=100)
    _plot_matrix(matrix, "Elementary Cellular Automaton")
