import numpy as np
import matplotlib.pyplot as plt


def _get_pixel(img, center, x, y):
    new_value = 0
    try: 
        if img[x][y] >= center:
            new_value = 1
    except:
        pass

    return new_value 

def _local_binary_pattern_pixel(img, x, y):
    '''
         64 | 128 |   1
        ----------------
         32 |   0 |   2
        ----------------
         16 |   8 |   4    
    '''

    center = img[x][y]
    val_ar = []
    val_ar.append(_get_pixel(img, center, x-1, y+1))   # top_right
    val_ar.append(_get_pixel(img, center, x, y+1))     # right
    val_ar.append(_get_pixel(img, center, x+1, y+1))   # bottom_right
    val_ar.append(_get_pixel(img, center, x+1, y))     # bottom
    val_ar.append(_get_pixel(img, center, x+1, y-1))   # bottom_left
    val_ar.append(_get_pixel(img, center, x, y-1))     # left
    val_ar.append(_get_pixel(img, center, x-1, y-1))   # top_left
    val_ar.append(_get_pixel(img, center, x-1, y))     # top

    power_val = [2**i for i in range(8)]

    val = 0
    for i in range(len(val_ar)):
        val += val_ar[i]*power_val[i]

    return val 

def local_binary_pattern(img):
    height, width = img.shape
    img_extend = np.zeros((height+2, width+2), dtype=np.uint8)
    img_extend[1:height+1, 1:width+1] = img

    
    img_lbp = np.zeros((height+2, width+2), np.uint8)

    for i in range(0, height+2):
        for j in range(0, width+2):
            img_lbp[i, j] = _local_binary_pattern_pixel(img_extend, i, j)
    
    img_lbp = img_lbp[1:height+1, 1:width+1] 
    hist_lbp, bins  = np.histogram(img_lbp, bins=range(0,256))

    return img_lbp, list(hist_lbp), list(bins)
